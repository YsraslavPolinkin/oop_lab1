﻿using System.ComponentModel;
using System.Text;

System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
customCulture.NumberFormat.NumberDecimalSeparator = ".";
System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

//Для підтримки української абетки треба додати такий код:
Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;


Console.WriteLine("Лабораторна робота №1\nВиконав: Полінкін Я.С., группа ІПЗ-22-2\nВаріант №15\nЗавдання 1.");



double a, b, c, D, x1, x2;

while (true)
{

    while (true)
    {
        Console.Write("Введіть дробове значення a = ");
        if (double.TryParse(Console.ReadLine(), out a)) break;
        Console.WriteLine("Помилка введенні значення a. Будь-ласка повторіть введення значення ще раз!");
    }

    while (true)
    {
        Console.Write("Введіть дробове значення b = ");
        if (double.TryParse(Console.ReadLine(), out b)) break;
        Console.WriteLine("Помилка введенні значення b. Будь-ласка повторіть введення значення ще раз!");
    }

    while (true)
    {
        Console.Write("Введіть дробове значення c = ");
        if (double.TryParse(Console.ReadLine(), out c)) break;
        Console.WriteLine("Помилка введенні значення c. Будь-ласка повторіть введення значення ще раз!");
    }


    D = Math.Pow(b, 2) - 4 * a * c;

    Console.WriteLine("D = " + D);

    if (D == 0)
    {
        x1 = -1 * b + Math.Sqrt(D);
        Console.WriteLine("x1 = " + x1);

    }

    if (D > 0)
    {
        x2 = -1 * b - Math.Sqrt(D);
        Console.WriteLine("x2 = " + x2);
    }

    if (D >= 0) break;

    Console.WriteLine("Помилка! Значення D < 0, D = " + D);
}